-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-11-2022 a las 01:15:52
-- Versión del servidor: 10.4.25-MariaDB
-- Versión de PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tecnoshop`
--
CREATE DATABASE IF NOT EXISTS `tecnoshop` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `tecnoshop`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `precio` float NOT NULL,
  `imagen` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` VALUES(1, 'Smart Watch Ticwatch E3', 41000, 'https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/t/i/ticwatch-e3.gif');
INSERT INTO `productos` VALUES(2, 'Notebook Asus X515ea hd - core i3 -11va -12gb - ssd256 - 15,6', 114000, 'https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/9/0/90nb0ty1-m00c20-12gb---foto-1-con-caracteristicas_1.jpg');
INSERT INTO `productos` VALUES(3, 'Impresora HP Ink tank 415 Multifunción Sistema continuo', 50000, 'https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/6/1/6199_1.jpg');
INSERT INTO `productos` VALUES(4, 'Pc Intel Core i5 10400 10ma. 8Gb 250 SSD', 85000, 'https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/p/c/pcstt-1012_cromax.jpg');
INSERT INTO `productos` VALUES(5, 'Smart Band Xiaomi Mi Band 6 Black', 7245, 'https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/m/i/miband6-1.jpg');
INSERT INTO `productos` VALUES(6, 'Mouse y mousepad gaming rgb talon elite', 1999, 'https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/m/o/mo-ter-wdotbk-0---foto-1.jpg');
INSERT INTO `productos` VALUES(7, 'Microfono de pie redragon - gm300 - blazar - interfaz usb', 14599, 'https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/1/3/13904_1.jpg');
INSERT INTO `productos` VALUES(8, 'Disco Solido Ssd 250gb M.2 Nvme 2300mb/s Pcie 3.0', 4932, 'https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/1/3/13813_1.jpg');
INSERT INTO `productos` VALUES(9, 'Disco Solido Ssd 240gb Bx500 Sata 2.5mm Crucial', 4750, 'https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/c/t/ct240bx500ssd1_bx500.jpg');
INSERT INTO `productos` VALUES(10, 'Notebook gamer asus 15.6\" fhd core i7-11370h 16gb 512gb tuf dash w11', 405102, 'https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/9/0/90nr05x1-m06720.jpg');
INSERT INTO `productos` VALUES(11, 'Notebook asus x515ea 15,6\" fhd core i7-1165G7 16gb ssd 512gb', 197230, 'https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/9/0/90nb0ty1-m011u0-16gb---foto-1-con-caracteristicas.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
